import React from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, Platform } from 'react-native';
import { Ionicons } from 'react-native-vector-icons';

export default function Header({ title, navigation }) {
  const open = () => {
    navigation.openDrawer();
  }

  return (
    <View style={styles.header}>
      <Ionicons  name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={28} onPress={open} style={styles.icon} />
      <View  style={styles.headerTitle}>
        <Text style={styles.headerText}>{title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333',
    letterSpacing: 1,
  },
  headerTitle: {
    flexDirection: 'row'
  },
  icon: {

  }
});