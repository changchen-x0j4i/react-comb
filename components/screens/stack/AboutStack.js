import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { Ionicons } from 'react-native-vector-icons';

import Header from '../../shared/Header';
import AboutScreen from '../AboutScreen';

const screens = {
  AboutScreen: {
    screen: AboutScreen,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {        
        headerTitle: () => <View style={css.headerTitle}>
          <Text style={css.headerText}>About</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: () => <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: () => <View />
      }
    }
  }
}

const AboutStack = createStackNavigator(screens, {
});

export default AboutStack;

const css = StyleSheet.create({
  headerTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  icon: {    
    marginLeft: 16,    
  }
});