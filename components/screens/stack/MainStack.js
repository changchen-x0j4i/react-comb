import { createStackNavigator } from 'react-navigation-stack';
import React from 'react';
import Header from '../../shared/Header';
import HomeScreen from '../HomeScreen';

const screens = {
  HomeScreen: {
    screen: HomeScreen,
    navigationOptions:({ navigation }) => {
      return {
        headerTitle:()=><Header title='Home' navigation={navigation}/>
      }
    }
  }
}

const MainStack = createStackNavigator(screens, {
  defaultNavigationOptions: {
    headerTintColor: '#444',
    headerStyle: {
      backgroundColor: '#eee',
      height: 60
    }
  }
})

export default MainStack;