import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import { Ionicons } from 'react-native-vector-icons';

import HomeScreen from '../screens/HomeScreen';
import ContactScreen from '../screens/ContactScreen';
import MyAccountScreen from '../screens/MyAccountScreen';
import SettingsScreen from '../screens/SettingsScreen';
import DrawerNavigator from './DrawerNavigator'


const screens = {
  Home: {
    screen: HomeScreen,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {
        //headerTitle:()=><Header title='About' navigation={navigation}/>
        headerTitle: <View style={css.headerTitle}>
          <Text style={css.headerText}>About</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: <View />
      }
    }
  },
  Contact: {
    screen: ContactScreen,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {
        //headerTitle:()=><Header title='About' navigation={navigation}/>
        headerTitle: <View style={css.headerTitle}>
          <Text style={css.headerText}>About</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: <View />
      }
    }
  },
  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {
        //headerTitle:()=><Header title='About' navigation={navigation}/>
        headerTitle: <View style={css.headerTitle}>
          <Text style={css.headerText}>About</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: <View />
      }
    }
  },
  Settings: {
    screen: SettingsScreen,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {        
        headerTitle: <View style={css.headerTitle}>
          <Text style={css.headerText}>About</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: <View />
      }
    }
  },

}

const AppStackNavigator = createStackNavigator(screens);

export default AppStackNavigator;

const css = StyleSheet.create({
  headerTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  icon: {    
    marginLeft: 16,    
  }
});