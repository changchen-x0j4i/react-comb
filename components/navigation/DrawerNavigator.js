import {createDrawerNavigator} from 'react-navigation-drawer';
import {createAppContainer} from 'react-navigation';

import MainStack from '../screens/stack/MainStack';
import AboutStack from '../screens/stack/AboutStack';
import AppTopNavigator from './TopTabNavigator'
import AppNavigator from './BottomTabNavigator';


const DrawerNavigator = createDrawerNavigator({
  
  App: {
    screen: AppTopNavigator
  },
  Main: {
    screen: AppNavigator
  },
  About: {
    screen: AboutStack,
  }
});

export default DrawerNavigator;