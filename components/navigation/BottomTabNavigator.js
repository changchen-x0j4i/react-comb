import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Ionicons } from 'react-native-vector-icons';

import HomeScreen from '../screens/HomeScreen';
import ContactScreen from '../screens/ContactScreen';
import MyAccountScreen from '../screens/MyAccountScreen';
import SettingsScreen from '../screens/SettingsScreen';

const Navigator = createBottomTabNavigator(
{
  Home: { 
    screen: HomeScreen,
    navigationOptions: {
      title: 'Home',
      headerTitle: "Upload", 
      tabBarLabel: 'Home',
      headerStyle: {
        backgroundColor: '#f4511e',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      tabBarIcon:({tintColor, focused}) => (
        <Ionicons 
          name = {(Platform.OS === 'ios') ? 'ios-home' : 'md-home'}
          size = {26}
          style = {{color: tintColor}}          
        />
      )
    }
  },
  Contact: {
    screen: ContactScreen,
    navigationOptions: {
      title: 'Contact',
      tabBarLabel: 'Contact',
      tabBarIcon:({tintColor, focused}) => (
        <Ionicons 
          name = {(Platform.OS === 'ios') ? 'ios-paper' : 'md-paper'}
          size = {26}
          style = {{color: tintColor}}          
        />
      )
    }
  },
  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions: {
      title: 'My Account',
      tabBarLabel: 'My Account',
      tabBarIcon:({tintColor, focused}) => (
        <Ionicons 
          name = {(Platform.OS === 'ios') ? 'ios-person' : 'md-person'}
          size = {26}
          style = {{color: tintColor}}          
        />
      )
    }
  },
  Settings: { 
    screen: SettingsScreen,
    navigationOptions: {
      title: 'Settings',
      tabBarLabel: 'Settings',
      tabBarIcon:({tintColor, focused}) => (
        <Ionicons 
          name = {(Platform.OS === 'ios') ? 'ios-options' : 'md-options'}
          size = {26}
          style = {{color: tintColor}}          
        />
      )
    }
  }
},
{
  tabBarOptions: {
    activeTintColor: '#33CAFF',
    inactiveTintColor: 'gray'
  }
});

const AppNavigator = createStackNavigator({
  AppNavigator: {
    screen: Navigator,
    navigationOptions:({ navigation }) => {
      const open = () => {
        navigation.openDrawer();
      }
      return {        
        headerTitle: () => <View style={css.headerTitle}>
          <Text style={css.headerText}>Main</Text>
        </View>,
        headerTitleStyle: {
          flex: 1,
          textAlign: 'center'
        },
        headerLeft: () => <TouchableOpacity>
          <Ionicons name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'} size={30} onPress={open} style={css.icon} />
        </TouchableOpacity>,
        headerRight: () => <View />
      }
    }
  }
});

export default AppNavigator;

const css = StyleSheet.create({
  headerTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  icon: {    
    marginLeft: 16,    
  }
});