import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import Page1Screen from '../screens/Page1Screen';
import Page2Screen from '../screens/Page2Screen';

const AppTopNavigator = createMaterialTopTabNavigator(
{
  Page1Screen: {
    screen: Page1Screen,
    navigationOptions: {      
      tabBarLabel: 'Page1'
    }
  },
  Page2Screen: {
    screen: Page2Screen,
    navigationOptions: {
      tabBarLabel: 'Page2'
    }
  },
},
{
  tabBarOptions: {
    style: {
      backgroundColor: "#678" //TabBar的背景色
    }
  }
});

export default AppTopNavigator;