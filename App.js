import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import AppStackNavigator from './components/navigation/AppStackNavigator';
import AppTopNavigator from './components/navigation/TopTabNavigator';
import AppNavigator from './components/navigation/BottomTabNavigator';
import DrawerNavigator from './components/navigation/DrawerNavigator';


// const Stack = createStackNavigator({
//   Stack: {
//     screen: DrawerNavigator,
//     navigationOptions: { title: 'Main' }
//   }
// });

const App = createAppContainer(DrawerNavigator);
export default App;